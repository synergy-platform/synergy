<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Packages that we only want to register if we are
        // in the local development environment
        if ($this->app->isLocal()) {

            // IDE Helper - https://github.com/barryvdh/laravel-ide-helper
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);

            // Debug Bar - https://github.com/barryvdh/laravel-debugbar
            if (env('DEBUG') == true) {
                $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            }
        }
    }
}
